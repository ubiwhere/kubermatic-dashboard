FROM quay.io/kubermatic/dashboard:v2.17.3

COPY logo-dark.svg  /dist/assets/images/branding/logo.svg
COPY favicon.ico  /dist/favicon.ico
